export interface CSSDeclaration extends Partial<CSSStyleDeclaration> {
    [name: string]: any
};

let sheet: CSSStyleSheet;

function hyphenate(name: string) {
    return name.replace(/[A-Z]|^ms/g, '-$&').toLowerCase();
}

function formatMedia(rule: string, media: string) {
    return media ? `${media}{${rule}}` : rule;
}

function formatRule(className: string, prop: string, value: string) {
    return `.${className}{${hyphenate(prop)}:${value}}`;
}

function parse(obj: CSSDeclaration, child: string, media: string, className: string) {
    for (var prop in obj) {
        const value = obj[prop];

        if (typeof value === "object") {
            const _media = /^@/.test(prop) ? prop : null;
            const _child = _media ? child : child + (
                /^&/.test(prop) ? prop.substring(1) : " " + prop
            );

            parse(value, _child, _media || media, className);
        } else {
            const rule = formatMedia(formatRule(className + child, prop, value), media);

            sheet.insertRule(rule, sheet.cssRules.length);

            console.log(rule);
        }
    }
}

export function css(obj: CSSDeclaration) {
    sheet = sheet || document.head.appendChild(document.createElement("style")).sheet as CSSStyleSheet;

    return parse(obj, '', '', 'x' + (sheet.cssRules.length).toString(36));
}
