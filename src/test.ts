import * as dom from "jsdom-sandbox";
import * as test from "tape";
import { css } from "./cheap";

test("foo", ({ equal, end }) => {
    dom.sandbox("<div id='foo'>bar</div>", {}, () => {

        css({
            color: "red",
            "h1": {
                color: "blue",
                "&:hover": {
                    color: "green"
                }
            },
            "@media screen and (min-width: 400px)": {
                "h1": {
                    fontSize: "50px"
                }
            }
        });

        end();
    });
});
